Geany autocomplete tags for Allegro library.

Based on headers from Allegro 5.2.6

* allegro5/*.h
* allegro5/inline/*.h
* allegro5/platform/*.h

I dont place tags from internal/alconfig.h because it's intresting only for
Allegro library developers (not me). I dont place tags from opengl/*.h because
it looks like standard OpenGL function names (it's just a guess).

Changelog:

* 0.9: Tags from math inlines and platform specific.
* 0.7: Tags from allegro addons.
* 0.5: Tags from allegro.h and it's includes except ALLEGRO_EXTRA_HEADERS and
       addons.

